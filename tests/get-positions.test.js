const getPositions = require('../lib/get-positions');

test('Get positions in a word for a given letter', () => {
  expect(getPositions('testing', 't')).toEqual([0, 3]);
})
